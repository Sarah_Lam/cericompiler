//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <cstring>
#include <set>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE {INTEGER, BOOLEAN, DOUBLE, CHAR};
// NB : it is better to use enum TYPE instead of TYPE to declare a type because TYPE is just a tag and not a type, but C++ identifies it as a type


TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, enum TYPE> DeclaredVariables;
unsigned long long TagNumber=0;

bool IsDeclared(const char *id){ // map (kind of associative array here) that stores an identifier wwith its type 
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s)
{
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

//Keyword := "if" | "then" | "else" | "for" | "while" | "begin" | "to" | "downto" | "do" | "end" | "display"
	
enum TYPE Expression(void) ;
void Statement(void) ;
void StatementPart(void) ;
	
enum TYPE Identifier(void){
	enum TYPE type ;
	if(!IsDeclared(lexer->YYText()))
	{
		cerr<<"Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl ;
		exit(-1) ;
	}
	type = DeclaredVariables[lexer->YYText()] ;
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type ;
}

//Number has now to deal with decimal numbers
enum TYPE Number(void)
{
	bool decimal = false ;
	double d ;
	unsigned int *i ;
	string number = lexer->YYText() ;
	if(number.find(".")!=string::npos) //there's the character '.' in the number, it's a decimal number
	{
		d=atof(lexer->YYText()) ; //converts number from char* to float
		i=(unsigned int *) &d; // i points to the const double
		//cout <<"\tpush $"<<*i<<"\t# Conversion of "<<d<<endl;
		// Is equivalent to : 
		cout<<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl ;
		cout<<"\tmovl	$"<<*i<<", (%rsp)\t# Conversion of "<<d<<" (32 bit high part)"<<endl ;
		cout<<"\tmovl	$"<<*(i+1)<<", 4(%rsp)\t# Conversion of "<<d<<" (32 bit low part)"<<endl ;
		current=(TOKEN) lexer->yylex() ;
		return DOUBLE ;
	}
	else //number doesn't contain '.', it's an integer
	{
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl ;
		current=(TOKEN) lexer->yylex() ;
		return INTEGER ;
	}
}

enum TYPE CharConst(void)
{
	cout<<"\tmovq $0, %rax"<<endl ;
	
	cout<<"\tmovq $"<<lexer->YYText()<<", %al"<<endl ;
	cout<<"\tpush %rax\t# push a 64-bit version of "<<lexer->YYText()<<endl ;
	current=(TOKEN) lexer->yylex();
	return CHAR ;
}

enum TYPE Factor(void)
{
	enum TYPE type;
	switch(current)
	{
		case RPARENT:
			current=(TOKEN) lexer->yylex();
			type=Expression();
			if(current!=LPARENT) Error("')' était attendu") ;
			else current=(TOKEN) lexer->yylex();
			break;
		case NUMBER:
			type=Number();
			break;
		case ID:
			type=Identifier();
			break;
		case CHARCONST:
			type=CharConst();
			break;
		default:
			Error("'(', ou constante ou variable attendue.") ;
	};
	return type;

}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPE Term(void){
	TYPE type, type1 ;
	OPMUL mulop;
	type=Factor();
	while(current==MULOP)
	{
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type1=Factor();
		if(type1!=type) Error("les facteurs ne sont pas du bon type") ;
		
		switch(mulop)
		{
			case AND:
				if(type1!=BOOLEAN) Error("Facteurs booléens attendu") ;
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\tmulq	%rbx"<<endl;	// a*b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(type1!=INTEGER && type1!=DOUBLE) Error("Facteurs numériques attendu") ;
				if(type1==INTEGER) //operands are integers
				{
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmulq	%rbx"<<endl;	// a*b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
				}
				else //operands are floats
				{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfmulp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
				}
				break;
			case DIV:
				if(type1!=INTEGER && type1!=DOUBLE) Error("Facteurs numériques attendu") ;
				if(type1==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfdivp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;
			case MOD:
				if(type1!=INTEGER) Error("Facteur de type entier attendu") ;
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type ;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPE SimpleExpression(void){
	enum TYPE type, type1 ;
	OPADD adop;
	type=Term();
	while(current==ADDOP)
	{
		adop=AdditiveOperator();		// Save operator in local variable
		type1=Term();
		if(type!=type1) Error("L'expression n'est pas du bon type") ;
		switch(adop)
		{
			case OR:
				if(type1!=BOOLEAN) Error("Type BOOLEAN attendu") ;
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\torq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				cout << "\tpush %rax"<<endl;			// store result
				break ;				
			case ADD:
				if(type1!=INTEGER && type1!=DOUBLE) Error("opérande non numérique pour l'addition");
				if(type1==INTEGER)
				{
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;			// store result
				}
				else
				{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}	
				break ;	
			case SUB:	
				if(type1!=INTEGER && type1!=DOUBLE) Error("opérande non numérique pour la soustraction");
				if(type1==INTEGER)
				{
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tsubq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;			// store result
				}
				else
				{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfsubp	%st(0),%st(1)\t# %st(0) <- op1 - op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;	
			default:
				Error("opérateur additif inconnu");
		}
	}
	return type ;
}

//returns the type according to the type given in the program
enum TYPE Type(void)
{
	if(current != KEYWORD) Error("type attendu") ;
	if(strcmp(lexer->YYText(), "BOOLEAN")==0)
	{
		current=(TOKEN) lexer->yylex() ;
		return BOOLEAN ;
	}
	else if(strcmp(lexer->YYText(), "INTEGER")==0)
	{
		current=(TOKEN) lexer->yylex() ;
		return INTEGER ;
	}
	else if(strcmp(lexer->YYText(), "DOUBLE")==0)
	{
		current=(TOKEN) lexer->yylex() ;
		return DOUBLE ;
	}
	else if(strcmp(lexer->YYText(), "CHAR")==0)
	{
		current=(TOKEN) lexer->yylex() ;
		return CHAR ;
	}
	else Error("type inconnu") ;
}

// Declaration := Ident {"," Ident} ":" Type
void Declaration(void)
{
	set<string> idents ;
	enum TYPE type ;
	if(current!=ID) Error("Un identificater était attendu");
	idents.insert(lexer->YYText()) ;
	current=(TOKEN) lexer->yylex() ;
	while(current==COMMA)
	{
		current=(TOKEN) lexer->yylex() ;
		if(current!=ID) Error("Un identificateur était attendu") ;
		idents.insert(lexer->YYText()) ;
		current=(TOKEN) lexer->yylex() ;
	}
	if(current!=COLON) Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex(); 
	type=Type() ;
	for (set<string>::iterator it=idents.begin(); it!=idents.end(); ++it)
	{
	    switch(type)
	    {
			case BOOLEAN:
			case INTEGER:
				cout << *it << ":\t.quad 0"<<endl ;
				break;
			case DOUBLE:
				cout << *it << ":\t.double 0.0"<<endl ;
				break;
			case CHAR:
				cout << *it << ":\t.byte 0"<<endl ;
				break;
			default:
				Error("type inconnu") ;
		};
		DeclaredVariables[*it]=type;
	}

}

//DeclarationPart := "var" VarDeclaration {";" Declaration} "."
void DeclarationPart(void)
{
	current=(TOKEN) lexer->yylex() ;
	Declaration() ;
	while(current==SEMICOLON)  //if we have other variables to delcare
	{
		current=(TOKEN) lexer->yylex() ;
		Declaration() ;
	}
	if(current!=DOT) Error("'.' attendu") ;
	current=(TOKEN) lexer->yylex() ;
	
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPE Expression(void){
	enum TYPE type, type1 ;
	unsigned long long TagEx ;
	OPREL oprel ;
	type=SimpleExpression();
	if(current==RELOP)
	{
		TagEx=++TagNumber ;
		oprel=RelationalOperator();
		type1=SimpleExpression() ;
		if(type1!=type) Error("Les expressions sont de types incompatibles.") ;
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel)
		{
			case EQU:
				cout << "\tje Vrai"<<TagEx<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<TagEx<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<TagEx<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<TagEx<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<TagEx<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<TagEx<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagEx<<endl;
		cout << "Vrai"<<TagEx<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagEx<<":"<<endl;
		return BOOLEAN ;
	}
	return type ;
}


// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	enum TYPE type, type1 ;
	string variable;
	if(current!=ID) Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText()))
	{
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText(); 
	type=DeclaredVariables[variable] ; //we get the variable's type as it is stored in the array DeclaredVariables
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN) Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type1=Expression();
	if(type!=type1) Error("Types incompatibles dans l'affectation") ;
	cout << "\tpop "<<variable<<endl;
}

//IfStatement := "if" Expression "then" Statement [ "else" Statement ]
void IfStatement(void){
	//when we want several if inside another one, we need to remember the first one using a local variable (TagIF)
	//otherwise we would erase TagNumber
	unsigned long long TagIF=++TagNumber ;
	current=(TOKEN) lexer->yylex() ;
	Expression() ;
	cout<<"pop %rax\t #Expression result"<<endl ; //we get in rax the result of expression
	cout<<"cmpq $0, %rax"<<endl ; //if it's 0 (false) we execute the else
	cout<<"\t je ELSE"<<TagIF<<endl ; 
	if (strcmp(lexer->YYText(), "then")!=0) Error("'then' attendu") ;
	current=(TOKEN) lexer->yylex();
	Statement() ;
	cout<<"\t jmp Suite"<<TagIF<<endl ;
	if (strcmp(lexer->YYText(), "else")==0) 
	{
		current=(TOKEN) lexer->yylex();
		cout<<"ELSE"<<TagIF<<":" ;
		Statement() ;
	}
	cout<<"Suite"<<TagIF<<":"<<endl ;
} 

//WhileStatement := "while" Expression "do" Statement
void WhileStatement(void){
	unsigned long long TagW=TagNumber++;
	cout<<"WHILE"<<TagW<<":"<<endl;
	current=(TOKEN) lexer->yylex();
	if(Expression()!=BOOLEAN)
		Error("expression booléene attendue");
	cout<<"\tpop %rax\t# Get the result of expression"<<endl;
	cout<<"\tcmpq $0, %rax"<<endl;
	cout<<"\tje ENDWHILE"<<TagW<<"\t# if FALSE, jump out of the loop"<<TagW<<endl;
	if(current!=KEYWORD||strcmp(lexer->YYText(), "do")!=0)
		Error("mot-clé DO attendu");
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"\tjmp WHILE"<<TagW<<endl;
	cout<<"ENDWHILE"<<TagW<<":"<<endl;
}

//ForStatement := "for" AssignementStatement "to" Expression "do" Statement 
//Add | "for" AssignementStatement "downto" Expression "do" Statement for decrementation
void ForStatement(void){
	if (strcmp(lexer->YYText(), "for")!=0) Error("for attendu") ;
	current=(TOKEN) lexer->yylex();
	AssignementStatement() ;
	if (strcmp(lexer->YYText(), "to")!=0) Error("'to' attendu") ;
	current=(TOKEN) lexer->yylex();
	Expression() ;
	if (strcmp(lexer->YYText(), "do")!=0) Error("'do' attendu") ;
	current=(TOKEN) lexer->yylex();
	Statement() ;
}

//BlockStatement := "begin" Statement {";" Statement} "end"
void BlockStatement(void){
	if (strcmp(lexer->YYText(), "begin")!=0) Error("begin attendu") ;
	current=(TOKEN) lexer->yylex();
	Statement() ;
	while (current == SEMICOLON) 
	{
		current=(TOKEN) lexer->yylex();
		Statement() ;
	}
	if (strcmp(lexer->YYText(), "end")!=0) Error("end attendu") ;
	current=(TOKEN) lexer->yylex();
}

//DisplayStatement := "display" Expression
void DisplayStatement()
{
	enum TYPE type ;
	unsigned long long TagD=++TagNumber ;
	current=(TOKEN) lexer->yylex() ;
	type=Expression() ;
	switch(type)
	{
		case INTEGER:
			cout << "\tpop %rsi\t# The value to be displayed"<<endl;
			cout << "\tmovq $FormatString1, %rdi\t# \"%llu\\n\""<<endl;
			cout << "\tmovl	$0, %eax"<<endl;
			cout << "\tcall	printf@PLT"<<endl;
			break;
		case BOOLEAN:
				cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
				cout << "\tcmpq $0, %rdx"<<endl;
				cout << "\tje False"<<TagD<<endl;
				cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
				cout << "\tjmp Next"<<TagD<<endl;
				cout << "False"<<TagD<<":"<<endl;
				cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
				cout << "Next"<<TagD<<":"<<endl;
				cout << "\tcall	puts@PLT"<<endl;
				break;
		case DOUBLE:
				cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
				cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
				cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
				cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
				cout << "\tmovq	$1, %rax"<<endl;
				cout << "\tcall	printf"<<endl;
				cout << "nop"<<endl;
				cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
				break;
		case CHAR:
				cout<<"\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si"<<endl;
				cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\""<<endl;
				cout << "\tmovl	$0, %eax"<<endl;
				cout << "\tcall	printf@PLT"<<endl;
				break;
		default:
				Error("DISPLAY ne fonctionne pas pour ce type de donnée.");
	}
}


// Statement := AssignementStatement
void Statement(void){
	if(current==KEYWORD) {
		if (strcmp(lexer->YYText(), "if")==0) //sub char by char, if all chars are the same the result will be 0, otherwise it'll be a negative or a positive value
			IfStatement() ;
		else if (strcmp(lexer->YYText(), "while")==0)
			WhileStatement() ;
		else if (strcmp(lexer->YYText(), "for")==0)
			ForStatement() ;
		else if (strcmp(lexer->YYText(), "begin")==0) 
			{BlockStatement() ;
			cout<<"ici"<<endl;}
		else if (strcmp(lexer->YYText(), "display")==0)
			DisplayStatement() ;
		else 
			Error("mot clé inconnu") ;
	}
	else 
		AssignementStatement();
}
	
// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==KEYWORD && strcmp(lexer->YYText(), "var")==0)
		DeclarationPart();
	StatementPart();	
}


int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	cout << "FormatString1:\t.string \"%llu\\n\"\t# used by printf to display 64-bit unsigned integers"<<endl ; 	
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





