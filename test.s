			# This code was produced by the CERI Compiler
FormatString1:	.string "%llu\n"	# used by printf to display 64-bit unsigned integers
FalseString:	.string "FASLE\n"
TrueString:		.string "TRUE\n"
a:	.quad 0
b:	.quad 0
c:	.quad 0
d:	.quad 0
denum:	.double 0.0
frac:	.double 0.0
num:	.double 0.0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $10
	pop b
	push $2
	pop c
	push b
	push c
	pop %rbx
	pop %rax
	movq $0, %rdx
	div %rbx
	push %rax	# DIV
	pop d
	push $1
	pop a
WHILE0:
	push d
	push $1
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	ja Vrai2	# If above
	push $0		# False
	jmp Suite2
Vrai2:	push $0xFFFFFFFFFFFFFFFF		# True
Suite2:
	pop %rax	# Get the result of expression
	cmpq $0, %rax
	je ENDWHILE0	# if FALSE, jump out of the loop0
	push d
	pop %rsi	# The value to be displayed
	movq $FormatString1, %rdi	# "%llu\n"
	movl	$0, %eax
	call	printf@PLT
	jmp WHILE0
ENDWHILE0:
	push a
	pop %rsi	# The value to be displayed
	movq $FormatString1, %rdi	# "%llu\n"
	movl	$0, %eax
	call	printf@PLT
	push c
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop c
	push b
	push c
	pop %rbx
	pop %rax
	movq $0, %rdx
	div %rbx
	push %rax	# DIV
	pop d
	push a
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop a
	push a
	push $3
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	ja Vrai6	# If above
	push $0		# False
	jmp Suite6
Vrai6:	push $0xFFFFFFFFFFFFFFFF		# True
Suite6:
	pop %rdx	# Zero : False, non-zero : true
	cmpq $0, %rdx
	je False5
	movq $TrueString, %rdi	# "TRUE\n"
	jmp Next5
False5:
	movq $FalseString, %rdi	# "FALSE\n"
Next5:
	call	puts@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
